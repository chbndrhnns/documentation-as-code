# Testing vuepress capabilities

| Tables        |      Are      |  Cool |
| ------------- | :-----------: | ----: |
| col 3 is      | right-aligned | $1600 |
| col 2 is      |   centered    |   $12 |
| zebra stripes |   are neat    |    $1 |

## How to 1

## Create a sequence diagram

@startuml
strict digraph meme {
exists [color=blue]
authenticate [color=blue]
require
create
UserCreated
destroy
UserDestroyed
get [color=blue]
authenticate -> require
create -> UserCreated
destroy -> require
destroy -> UserDestroyed
get -> require
}
@enduml

## Mermaid support

<mermaid>
graph TD
  A[Cool] -->|Get money| B(Go shopping)
  B --> C{Let me}
  C -->|Two| D[Laptop]
  C -->|Two| E[iPhone]
  C -->|Three| F[Car]
  C -->|Four| F[Mac]
</mermaid>

## Timeline support

<sample-timeline/>

## Table support

We can even search in tables!

<demo-table/>

## Resolved tickets as a chart

<pie-chart :data="[['Blueberry', 44], ['Strawberry', 23]]" :download="true" download="test"></pie-chart>

---
sidebar: auto
---

# Documentation-as-code

## Idea

Apply practices from developing code to writing documentation.

## Advantages

Documentation has traditionally been handled in a different way from source code: Authored and created with a word processing tool and often converted to PDF for presentation or distribution. In contrast, source code is often managed in a version-controlled repository. Applying the principles found in the handling of source code to the domain of documentation is what we call _documentation-as-code_.

Here are some advantages of storing plain text using a version control system:

### Readability

While not everyone is accustomed to using a text editor as part of daily life, plain text is universally readable. No special computer program is required to display it.

### Single Source of Truth

The version control system contains the most current version of the content - whenever in doubt if the version you have is up-to-date, just take a look at the repository and, if necessary, fetch the changes.

Sending around email attachments with content you are creating is basically no longer necessary.

### Collaboration

For every change that has been made to a document, it is clearly visible _who_ contributed _what_ part at _which time_. A best practice is also to provide the reasoning behind a change. By design, changes coming from different authors can easily be integrated by merging them. If two authors made changes to the same line, there is a merge conflict that needs to be resolved manually.

### Versioning

Have you ever come across a filename like `20180422 howto_v0.99-2.docx` that, when you openend it, contained a totally different version number in the footer? Using source control provides versioning by nature. If stored as plain text, different versions can be compared to see differences.
Changes to content stored in a repository are made by `committing` them which creates a **commit** that uniquely identifies a set of changes.

Binary formats like `docx` or `pdf` are a bad fit for version-controlled storage as differences cannot be computed without knowledge about the internals of a binary format.

### Inherent backup

A complete version of repository (including the history) exists on every client that has downloaded the repository. Accidentally deleting it does not make it go away and you can restore it by downloading a copy again.

### Contribution

If a piece of content is available as sources for everyone, this is an invitation to contribute to the content. Be it correcting errors, improving the language or contributing updated content - it is a no-brainer: you download the repository, commit your changes, and wait for the changes to be accepted.

## Difficulties

If version control offers so much advantages, why is not in use more broadly? To be honest, I do not know.

Besides plain text, documentation will often also be comprised of

- images,
- illustrations or
- diagrams.

For these components, finding good ways to store them as plain text can be a challenge.

## Requirements

The following things are required to get started with _documentation-as-code_:

- a text editor
- a repository
- a client to control the repository

In the setup where this document was created, the following components are in place:

- a text editor (recommendation: [Visual Studio Code](https://code.visualstudio.com/)),
- a current node.js ^[cf. [https://nodejs.org/en/](https://nodejs.org/en/)] installation
- a Git repository ^[cf. [https://git-scm.com/](https://git-scm.com/)],
- a Gitlab ^[cf. <https://gitlab.com/>] instance with
  - Gitlab CI enabled that execute continuous integration jobs
  - Gitlab Pages for hosting the HTML version of the documentation
- Vuepress ^[cf. [https://vuepress.vuejs.org/](https://vuepress.vuejs.org/)] for rendering the Markdown
  files into beautiful web sites,

## Architecture

To explain the architecture, let's start with a diagram that shows the interaction between the different components.

@startuml

actor user as user
entity md as md

box "local"
control "local builder" as builder_local
entity browser as browser
end box

database repo as repo

box "Continuous Integration Chain"
control "CI runner" as runner
control "remote builder" as builder_remote
entity html as html
end box
control webserver as web

user->md: write
activate md

md->builder_local: update docs
builder_local->browser: display docs

...

user->repo: commit changes
activate repo
repo->runner: trigger CI job
runner->builder_remote: trigger build
builder_remote->html: create build artifact
builder_remote-->runner: notify success
runner->html: request artifact
html-->runner: send artifact

runner->web: publish artifact
deactivate md

...

user->web: browse docs

@enduml

## Sample Walk-Through (local editing)

### 1. Prepare your environment

Take a look at the [requirements](#requirements) section for details.

If `vuepress` is not yet running on your machine, you can install it via `yarn add vuepress`.

### 2. Write your content in a text editor

Fire up an editor and write the content.

![Visual Studio Code ](./assets/vs-code.png)

### 3. Commit your changes

Confirm the changes and commit them.

![Changes compared to the repository document](./assets/diff.png)

### 4. Building artifacts

Vuepress continuously watches your source files for changes and creates an HTML version on-demand.

To start `vuepress`, run `vuepress dev`

![Vuepress builds the content on changes](./assets/build.png)

### 5. View result

This HTML version can be viewed in your web browser. The url is displayed by `vuepress` when the rendering has been completed.

![Browser showing the rendered documentation](./assets/browser.png)

---
sidebar: auto
---

# Hello, notebook! :tada:

::: tip

Want to see quick wins? Take a look at [Documentation-as-Code](how-to/docs-as-code.md).

The content you see here has been created using the Markdown language which can be learned in around 10 minutes^[[https://www.attosol.com/learn-markdown/](https://www.attosol.com/learn-markdown/)].

:::

## Published documentation

The current version of this documentation is available at [https://chbndrhnns.gitlab.io/documentation-as-code/](<https://chbndrhnns.gitlab.io/documentation-as-code/)

## Getting started

To work on this repository locally, clone it and run `yarn` or `npm install` to install the required dependencies.

You can use a text editor of your choice to edit the files and I highly recommend **Visual Studio Code**^[[https://code.visualstudio.com/](https://code.visualstudio.com/)].

A local preview of the rendered documentation that is updated automatically on changes can started with `yarn docs:serve` (or if you prefer npm, run `npm run docs:serve`).

## Contents

In this demo repo, I can only recommend two pages:

- [Documentation-as-Code](how-to/docs-as-code.md)
- [Capabilities of Vuepress](how-to/capabilities.md)

module.exports = {
  base: process.env.NODE_ENV === 'production' ? '/documentation-as-code/' : '/',
  title: 'Demo Notebook',
  theme: 'cool',
  description: '',
  dest: 'public',
  themeConfig: {
    // sidebar: 'false',
    nav: [{
      text: "How to's",
      link: '/how-to/',
    }],
    lastUpdated: 'Last Updated', // string | boolean
    editLinks: true,
    repo: 'https://gitlab.tools.in.pan-net.eu/e174358/memos',
  },
  markdown: {
    anchor: {
      permalink: true
    },
    toc: {
      includeLevel: [1, 2, 3, 4]
    },
    config: md => {
      md.set({
        breaks: true
      });
      md.use(require('markdown-it-katex'));
      md.use(require('markdown-it-anchor'));
      md.use(require('markdown-it-plantuml'));
      md.use(require('markdown-it-footnote'));
      md.use(require('markdown-it-implicit-figures'), {
        dataType: true, // <figure data-type="image">, default: false
        figcaption: true, // <figcaption>alternative text</figcaption>, default: false
        tabindex: true, // <figure tabindex="1+n">..., default: false
        link: false // <a href="img.png"><img src="img.png"></a>, default: false
      })

      // md.use(require('markdown-it-xxx'))
    },
  },
};